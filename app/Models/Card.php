<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as MongoModel;

class Card extends MongoModel
{
    protected $connection = 'mongodb';
    protected $collection = 'cards';
    protected $guarded = ['uuid', 'created_at', 'updated_at'];

    public function set()
    {
        return $this->belongsTo('App\Set');
    }

    public function setNamesAttribute($value)
    {
        $this->attributes['names'] = (!empty($value)) ? $value : [];
    }

    public function setColorsAttribute($value)
    {
        $this->attributes['colors'] = (!empty($value)) ? $value : [];
    }

    public function setSupertypesAttribute($value)
    {
        $this->attributes['supertypes'] = (!empty($value)) ? $value : [];
    }

    public function setTypesAttribute($value)
    {
        $this->attributes['types'] = (!empty($value)) ? $value : [];
    }

    public function setSubtypesAttribute($value)
    {
        $this->attributes['subtypes'] = (!empty($value)) ? $value : [];
    }

    public function setVariationsAttribute($value)
    {
        $this->attributes['variations'] = (!empty($value)) ? $value : [];
    }

    public function setColorIdentityAttribute($value)
    {
        $this->attributes['color_identity'] = (!empty($value)) ? json_encode($value) : '';
    }

    public function setTranslationsAttribute($value)
    {
        $this->attributes['translations'] = (!empty($value)) ? $value : [];
    }

    public function getNamesAttribute($value)
    {
        return $value;
    }

    public function getColorsAttribute($value)
    {
        return $value;
    }

    public function getSupertypesAttribute($value)
    {
        return $value;
    }

    public function getTypesAttribute($value)
    {
        return $value;
    }

    public function getSubtypesAttribute($value)
    {
        return $value;
    }

    public function getVariationsAttribute($value)
    {
        return $value;
    }

    public function getColorIdentityAttribute($value)
    {
        return json_decode($value);
    }

    public function getTranslationsAttribute($value)
    {
        return $value;
    }
}
