<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Set extends Eloquent
{

    protected $connection = 'mongodb';
    protected $collection = 'sets';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function cards()
    {
        // Return the relationship. Tell which model it relates to and what the identifying column is in that table.
        return $this->hasMany('App\Card');
    }

    /**
     * @param $value
     */
    public function setMagicRaritiesCodesAttribute($value)
    {
        $this->attributes['magic_rarities_codes'] = (!empty($value)) ? json_encode($value) : '';
    }

    /**
     * @param $value
     * @return mixed
     */
    public function getMagicRaritiesCodesAttribute($value)
    {
        return json_decode($value);
    }

    /**
     * @param $value
     */
    public function setBoosterAttribute($value)
    {
        $this->attributes['booster'] = (!empty($value)) ? json_encode($value) : '';
    }

    /**
     * @param $value
     */
    public function setTranslationsAttribute($value)
    {
        $this->attributes['translations'] = (!empty($value)) ? json_encode($value) : '';
    }

    /**
     * @param $value
     */
    public function setAlternativeNamesAttribute($value)
    {
        $this->attributes['alternative_names'] = (!empty($value)) ? json_encode($value) : '';
    }


    public function setNameAttribute($value)
    {
        $this->attributes['name'] = (!empty($value)) ? json_encode($value) : '';
    }

    /**
     * @param $value
     * @return mixed
     */
    public function getBoosterAttribute($value)
    {
        return json_decode($value);
    }

    /**
     * @param $value
     * @return mixed
     */
    public function getTranslationsAttribute($value)
    {
        return json_decode($value);
    }

    /**
     * @param $value
     * @return mixed
     */
    public function getAlternativeNamesAttribute($value)
    {
        return json_decode($value);
    }

    public function getNameAttribute($value)
    {
        return json_decode($value);
    }
}
