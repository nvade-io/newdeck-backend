<?php

namespace App\Repositories;

use App\Models\Card;
use Illuminate\Database\Eloquent\Model;

class CardRepository extends BaseRepository implements CardRepositoryInterface
{
    /**
     * @param  Model  $model
     */
    public function __construct(Card $model)
    {
        $this->model = $model;
    }

}
