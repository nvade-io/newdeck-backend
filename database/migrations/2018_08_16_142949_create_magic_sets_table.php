<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMagicSetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('code')->unique();
            $table->string('old_code')->nullable();
            $table->string('release_date')->nullable();
            $table->string('border')->nullable();
            $table->string('type')->nullable();
            $table->string('block')->nullable();
            $table->string('mkm_name')->nullable();
            $table->string('mkm_id')->nullable();
            $table->text('booster')->nullable();
            $table->string('magic_cards_info_code')->nullable();
            $table->string('gatherer_code')->nullable();
            $table->text('magic_rarities_codes')->nullable();
            $table->text('translations')->nullable();
            $table->text('online_only')->nullable();
            $table->text('alternative_names')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sets');
    }
}
