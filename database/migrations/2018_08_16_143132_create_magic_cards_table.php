<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMagicCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cards', function (Blueprint $table) {
            $table->increments('uuid');
            $table->string('id')->nullable();
            $table->string('layout')->nullable();
            $table->string('name');
            $table->string('names')->nullable();
            $table->string('mana_cost')->nullable();
            $table->string('cmc')->nullable();
            $table->json('colors')->nullable();
            $table->string('type')->nullable();
            $table->json('supertypes')->nullable();
            $table->json('types')->nullable();
            $table->json('subtypes')->nullable();
            $table->string('rarity')->nullable();
            $table->text('text')->nullable();
            $table->text('flavor')->nullable();
            $table->string('artist')->nullable();
            $table->string('number')->nullable();
            $table->string('power')->nullable();
            $table->string('toughness')->nullable();
            $table->string('loyalty')->nullable();
            $table->string('multiverseid')->nullable();
            $table->json('variations')->nullable();
            $table->string('image_name')->nullable();
            $table->string('watermark')->nullable();
            $table->string('border')->nullable();
            $table->string('hand')->nullable();
            $table->string('life')->nullable();
            $table->string('mci_number')->nullable();
            $table->string('release_date')->nullable();
            $table->string('reserved')->nullable();
            $table->json('translations')->nullable();
            $table->string('color_identity')->nullable();
            $table->string('timeshifted')->nullable();
            $table->string('starter')->nullable();

            $table->unsignedInteger('set_id');
            $table->foreign('set_id')->references('id')->on('sets')->onDelete('cascade');

            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cards');
    }
}
