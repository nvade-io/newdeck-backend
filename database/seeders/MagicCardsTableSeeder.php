<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Url;

class MagicCardsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        

        $set = json_decode(file_get_contents('http://localhost/json/AER.json'));


        $this->command->info('Seeding: ' . $set->name);

        $setData = $this->normalizeSetData($set);

        unset($setData['cards']);
        $magicSet = \App\Models\Set::create($setData);

        foreach($set->cards as $card) {
            $cardData = $this->normalizeCardData($card, $magicSet);
            \Log::info('Card info', $cardData);
            $magicCard = new \App\Models\Card($cardData);
            $magicCard->save();
        }

    }

    protected function normalizeSetData($set) {
        $arr = [];
        foreach ($set as $key => $value) {
            $arr[\Str::snake($key)] = $value;
        }

        \Log::info('Card info', $arr);
        return $arr;
    }
    protected function normalizeCardData($card, $magicSet) {
        $arr = [];
        foreach($card as $key => $value) {
            $arr[\Str::snake($key)] = $value;
        }
        $arr['set_id'] = $magicSet->id;
        return $arr;
    }
}
